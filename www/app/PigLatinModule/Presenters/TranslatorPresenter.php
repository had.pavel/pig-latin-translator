<?php

declare(strict_types=1);

namespace App\Presenters;

use App\PigLatinModule\Services\TranslatorService\PigLatinTranslator;
use Nette\Application\UI\Form;
use App\PigLatinModule\Forms\PigLatinTranslatorFormFactory;

final class TranslatorPresenter extends BasePresenter
{

    /**
     * @var PigLatinTranslatorFormFactory
     */
    private $formFactory;

    public function __construct(
        PigLatinTranslator            $translator,
        PigLatinTranslatorFormFactory $formFactory
    )
    {
        $this->formFactory = $formFactory;

        parent::__construct($translator);
    }

    public function createComponentTranslatorForm(string $name): Form
    {
        $form = $this->formFactory->create();
        $form->onSuccess[] = [$this, 'translatorFormSucceeded'];
        
        return $form;
    }

    public function translatorFormSucceeded(Form $form, $data): void
    {
        if ($this->isAjax()) {
            if (strlen($data->word) === 0) {
                $form['word']->addError('Slovo nesmí být prázdné!');
            } else {
                $finalTranslation = '';
                $words = explode(' ', $data->word);
                foreach ($words as $word) {
                    $finalTranslation .= $this->translator->translate($word) . ' ';
                }

                $this->template->translateWord = trim($finalTranslation);
            }

            $this->redrawControl('translateFormSnippet');
        }
    }

}