<?php

declare(strict_types=1);

namespace App\Presenters;

use App\PigLatinModule\Services\TranslatorService\PigLatinTranslator;
use Nette\Application\UI\Presenter;

class BasePresenter extends Presenter
{
    /**
     * @var PigLatinTranslator
     */
    protected $translator;

    /**
     * @param PigLatinTranslator $translator
     */
    public function __construct(PigLatinTranslator $translator)
    {
        $this->translator = $translator;
    }
}