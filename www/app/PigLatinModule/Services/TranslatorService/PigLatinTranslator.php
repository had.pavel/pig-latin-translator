<?php

namespace App\PigLatinModule\Services\TranslatorService;

use Nette\Localization\Translator;

/**
 * @package App\Services\TranslatorService
 */
class PigLatinTranslator implements Translator
{
    public const VOWELS = ['a', 'e', 'i', 'o', 'u', 'y'];

    public const ENDINGS = ['yay', 'hay', 'way'];

    function translate($message, ...$parameters): string
    {
        if (strlen($message) === 0) return '';

        if (in_array($message[0], $this::VOWELS)) {
            // vowel first

            $result = $message . '-' . $this->getRandomWordEnding();
        } else {
            //consonant first

            $vowelIndex = $this->findFirstIndexOfVowel($message);

            $result = substr($message, $vowelIndex, strlen($message) - $vowelIndex) .
                '-' .
                substr($message, 0, $vowelIndex);
            $result .= 'ay';
        }

        return $result;
    }

    /**
     * @param string $word
     * @return int
     */
    private function findFirstIndexOfVowel(string $word): int
    {
        $firstPosition = strlen($word);

        foreach ($this::VOWELS as $vowel) {
            $actualPosition = strpos($word, $vowel);
            if ($actualPosition !== false && $actualPosition < $firstPosition) {
                $firstPosition = $actualPosition;
            }
        }

        return $firstPosition;
    }

    /**
     * @return string
     */
    private function getRandomWordEnding(): string
    {
        return $this::ENDINGS[rand(0, count($this::ENDINGS) - 1)];
    }

}