<?php

declare(strict_types=1);

namespace App\PigLatinModule\Forms;

use Nette\Application\UI\Form;

class PigLatinTranslatorFormFactory
{

    public function create(): Form
    {
        $form = new Form;
        $form->getElementPrototype()->class('ajax');
        $form->addText('word', 'Slovo');
        $form->addSubmit('send', 'Přeložit');

        return $form;
    }

}