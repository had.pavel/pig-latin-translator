# Pig latin translator

Pig Latin je jazyková hra používaná anglicky mluvícími dětmi. Používá se pro pobavení i pro zachování „soukromí“ mezi mluvčími. Některá slova (např. upidstay - hloupý) byla přejata do slangu americké angličtiny. Celý princip hry spočívá v úpravě slov přidáním atypických přípon nebo přesunutím první souhlásky na konec původního slova.

## Installation

V hlavní složce projektu nastartujeme docker

```bash
docker compose up
```

Jdeme do www složky projektu a stáhneme potřebné závislosti

```bash
cd ./www
composer install
```

## Usage

Otevřeme si okno prohlížeče a [kocháme se tou nádherou](http://localhost:8081/)

## License
I don't mind